# Martin Papy maven repository

Welcome to my public artifacts repository. You can use it with maven, ivy or another compatible dependency manager.

## To use it with **maven**

In your ```pom.xml```, add the bellow snippet:
  
```
<repositories>
    <repository>
        <id>mpapy-bitbucket</id>
        <name>Martin Papy maven repository</name>
        <url>https://bitbucket.org/api/1.0/repositories/mpapy/maven-repo/raw/master</url>
        <layout>default</layout>
    </repository>
</repositories>
```
  
## To use it with **ivy**

Use a resolver like the following:

```
<ibiblio
    name="mpapy-bitbucket"
    root="https://bitbucket.org/api/1.0/repositories/mpapy/maven-repo/raw/master"
    m2compatible="true" />
```